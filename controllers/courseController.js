const Course = require("../models/Course");


// Controller Functions:

// Creating a new course
module.exports.addCourse = (userData, reqBody) => {
    if (userData == true) {
        let newCourse = new Course({
            name: reqBody.name,
            description: reqBody.description,
            price: reqBody.price
        });

        return newCourse.save().then((course, error) => {
            if (error) {
                return false //"Course creation failed."
            } else {
                return true
            }
        })
    }
    else { return false }

};


module.exports.getAllCourses = (data) => {
    if (data.isAdmin) {
        return Course.find({}).then(result => {
            return result

        })

    } else {
        return false
    }

};

module.exports.getAllActive = () => {
    return Course.find({ isActive: true }).then(result => {
        return result
    })
};

//Retrieving a specific course
module.exports.getAllCourses = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result
    })
};


//Update a specific course
module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((updatedCourse, error) => {

        console.log(updatedCourse)
        if (error) { return false }
        else { return true }

    }
    )


}

module.exports.archiveCourse = (reqParams, reqBody) => {
    let archivedCourse = {
        isActive: reqBody.isActive
    }
    return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((archivedCourse, error) => {

        console.log(archivedCourse)
        if (error) { return false }
        else { return true }

    }
    )


}

